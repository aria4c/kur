<?php
date_default_timezone_set('Europe/Kiev');
/*
 * Автозагрузка php по неймспейсам (безумно облегчает работу, можно забыть о include)
 */
spl_autoload_extensions(".php");
spl_autoload_register();
session_start();
use engine\controllers\ActionHandler;

$actionHandler = new ActionHandler(); //создаем хендлер страниц

$actionHandler->execute(isset($_GET['action']) ? $_GET['action'] : "none"); //передаем страницу для отображения