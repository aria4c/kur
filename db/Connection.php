<?php

namespace db;

use config\Config;

class Connection
{

    protected $mysqli;

    public function __construct()
    {
        $this->mysqli = new \mysqli(
            Config::DB_HOST, Config::DB_USER, Config::DB_PASS, Config::DB_NAME
        );
    }

    public function getVideos()
    {
        $query = "SELECT * FROM video";
        $result = $this->mysqli->query($query);

        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    public function getVideoById($id)
    {
        $query = "SELECT * FROM video WHERE id = $id";
        $result = $this->mysqli->query($query);
        if ($result) {
            $res = mysqli_fetch_all($result, MYSQLI_ASSOC);
            if (is_array($res)) {
                return $res[0];
            }
        }
        return null;
    }

    public function getUserByName($username)
    {
        $query = "SELECT * FROM admin WHERE username = '$username'";
        $result = $this->mysqli->query($query);
        if ($result) {
            $res = mysqli_fetch_all($result, MYSQLI_ASSOC);
            if (is_array($res)) {
                return $res[0];
            }
        }
        return null;
    }

    public function deleteVideoById($id)
    {
        $query = "DELETE FROM video WHERE id = '$id'";
        $result = $this->mysqli->query($query);
        return null;
    }

    public function editVideoById($id, $title, $content, $link)
    {
        $query = "UPDATE video SET name='$title', link='$link', content='$content' WHERE id=$id";
        $result = $this->mysqli->query($query);

        return null;
    }

    ///////////////
    /// Add Tag ///
    ///////////////

    public function AddTagForVideo($id, $tag)
    {
        //$res = mysqli_query("SELECT * FROM  video_tags WHERE video_id = $id AND tag_id = $tag");
        $query = "SELECT * FROM  video_tags WHERE video_id = $id AND tag_id = $tag";
        $result = $this->mysqli->query($query);
        $count = mysqli_num_rows($result);

        if ($count > 0) {
            // Есть данные
        } else {
            // нет данных
            $query = "INSERT INTO video_tags VALUE(0,'$id','$tag')";
            $result = $this->mysqli->query($query);
        }
        return null;
    }

    public function AddVideo($title, $content, $link)
    {
        $query = "INSERT INTO video VALUE(0,'$title','$link','$content')";
        $result = $this->mysqli->query($query);

        return null;
    }

    public function getVideosByQuery($query)
    {
        $query = "SELECT * FROM video WHERE name LIKE '%$query%' OR content LIKE '%$query%'";
        $result = $this->mysqli->query($query);

        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    public function getVideosByTag($tag)
    {
        $query = "SELECT video_id FROM video_tags WHERE tag_id=$tag";
        $result = $this->mysqli->query($query); // Класс для работы с БД возвращает не только результат запроса

        $result = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $video_ids = implode(',', array_map(function ($el) {
            return $el['video_id'];
        }, $result)); // если состоит из одного элемента - array map превращает в строку
        $string_res = is_array($video_ids) ? implode(',', $video_ids) : $video_ids;
        $result = "SELECT * FROM video WHERE id IN ($string_res)";
        $result = $this->mysqli->query($result);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    public function getTagByVideo($id)
    {
        $query = "SELECT tag_id FROM video_tags WHERE video_id = $id"; // Как на 51 строке
        $result = $this->mysqli->query($query); // Класс для работы с БД возвращает не только результат запроса
        if ($result) {
            $result = mysqli_fetch_all($result, MYSQLI_ASSOC);
            $tag_ids = implode(',', array_map(function ($el) {
                return $el['tag_id'];
            }, $result));
            $result = "SELECT * FROM tags WHERE tag_id IN ($tag_ids)";
            $result = $this->mysqli->query($result, MYSQLI_ASSOC);
        }

        return $result;
    }

    public function getTagIdByName($tagName)
    {
        $query = "SELECT tag_id FROM tags WHERE name='$tagName'";
        $result = $this->mysqli->query($query);
        if($result)
        {
            $res = mysqli_fetch_array($result);
            if(is_array($res))
            {
                return $res[0];
            }
        }
        return null;
    }

    public function assignTagToVideo($videoId, $tagId)
    {
        $query = "INSERT INTO video_tags VALUE (0,$videoId, $tagId)";
        $this->mysqli->query($query);
    }

    public function addTag($tagName)
    {
        $query = "INSERT INTO tags VALUE (0,'$tagName')";
        $this->mysqli->query($query, MYSQLI_USE_RESULT);
        return $this->mysqli->insert_id;
    }

    /**
     * Remove all tags for video
     *
     * @param $videoId
     */
    public function truncateTags($videoId)
    {
        $query = "DELETE FROM video_tags WHERE video_id=$videoId";
        $this->mysqli->query($query);
    }
}