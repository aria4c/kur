<?php


/*
 * Автозагрузка php по неймспейсам (безумно облегчает работу, можно забыть о include)
 */
spl_autoload_extensions(".php"); // Подгружает все классы все файлы php
spl_autoload_register();
session_start();
use engine\handlers\PageHandler;

$pageHandler = new PageHandler(); //создаем хендлер страниц

echo $pageHandler->handle(isset($_GET['page']) ? $_GET['page'] : "main"); //передаем страницу для отображения
// Если GET не передаюется по умолчанию подгружается main