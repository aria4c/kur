<?php
/** Сделано при моральной поддержке коллектива MiyaGi & Эндшпиль */

namespace config;

/**
 * Created by PhpStorm.
 * UserData: smile
 * Date: 20.06.17
 * Time: 14:12
 */

class Config {
    //Бэкэнд сайта (MySQL)
    const BACKEND = "MySQL";

    //Данные для подключения к БД.
    const DB_HOST = 'localhost';
    const DB_USER = 'root';
    const DB_PASS = '';
    const DB_NAME = 'kur';

    //Роутинг к исполняемому PHP файлу шаблона
    public static $siteRoutes = array(
        "main" => "engine\handlers\pages\MainPage",
        "edit" => "engine\handlers\pages\EditPage",
		"add" => "engine\handlers\pages\AddPage",
		"search" => "engine\handlers\pages\SearchPage",
        "tag_search" => "engine\handlers\pages\TagSearch"      ///
    );

    public static $adminRoutes = array(
      "main"=>"engine\handlers\admin\pages\AdminMainPage",
      "blog"=>"engine\handlers\admin\pages\BlogPage",
    );

    public static $actions = array(
        "login"=>"engine\controllers\LoginController",
		"add"=>"engine\controllers\AddController",
        "edit"=>"engine\controllers\EditController",
        "logout"=>"engine\controllers\LogoutController",
        "delete"=>"engine\controllers\DeleteController",
    );


    public static $adminVars = array(
        '{%%add_new%%}' => 'template/admin/elements/add.html',
        '{%%edit_button%%}' => 'template/admin/elements/edit.html',
        '{%%remove_button%%}' => 'template/admin/elements/delete.html'
    );
}