<?php
namespace engine\controllers;

class LogoutController extends AbstractController{

    public function execute()
    {
        session_start();
        session_destroy();
        unset($_SESSION);
        header("Location: /");
    }
}