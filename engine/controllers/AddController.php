<?php
namespace engine\controllers;

use db\Connection;

class AddController extends AbstractController{

    public function execute()
    {
        if(isset($_SESSION['loggedIn']))
        {
            $title= $_POST['title'];
            $link = 'https://www.youtube.com/embed/'.$_POST['link'];
            $content = $_POST['content'];
            $connection = new Connection();
            $connection->AddVideo($title,$content,$link);

            header('Location: /');
        }
    }
}