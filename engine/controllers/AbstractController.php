<?php
namespace engine\controllers;

use config\Config;
use engine\db\DatabaseManager;
use engine\db\IConnectable;
use engine\db\traits\ConnectionBroker;

abstract class AbstractController {
    public abstract function execute();
}
//