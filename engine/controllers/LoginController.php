<?php
namespace engine\controllers;

use db\Connection;

class LoginController extends AbstractController {

    private function doLogin($name){
        session_regenerate_id();
        $_SESSION['loggedIn'] = true;
        $_SESSION['ua'] = $_SERVER['HTTP_USER_AGENT'];
        $_SESSION['username'] = $name;
    }

    private function auth($name, $pass){
        $connection = new Connection();
        $user = $connection->getUserByName($name);
        if(!$user)
        {
            return false;
        }

        if($user['password'] === hash("sha256", $pass))
        {
            return true;
        }

        return false;
    }

    public function execute()
    {
        $name = $_POST['username'];
        $pass = $_POST['password'];

        if($this->auth($name, $pass))
        {
            $this->doLogin($name);
        }
        header('Location: /');
    }
}