<?php
namespace engine\controllers;

use config\Config;
use db\Connection;
use engine\db\models\AbstractModel;

class DeleteController extends AbstractController{

    public function execute()
    {
        if(isset($_SESSION['loggedIn']) && isset($_GET['id']))
        {
            $connection = new Connection();
            $connection->deleteVideoById($_GET['id']);

        }
        header("Location: /");
    }
}