<?php
namespace engine\controllers;

use db\Connection;

class EditController extends AbstractController{

    public function execute()
    {
        if(isset($_SESSION['loggedIn']))
        {
            $videoId = $_POST['id'];
            $title= $_POST['title'];
            $link = 'https://www.youtube.com/embed/'.$_POST['link'];
            $content = $_POST['content'];
            $connection = new Connection();
            $connection->editVideoById($videoId,$title,$content,$link);
            $tags = $_POST['tags'];
            $tags = preg_replace('/\s+/', '', $tags);
            $tagsArray = explode(',',$tags);
            $connection->truncateTags($videoId);
            foreach ($tagsArray as $tagName)
            {
                if($tagId = $connection->getTagIdByName($tagName)){
                    $connection->assignTagToVideo($videoId, $tagId);
                } else
                {
                    $newTagId = $connection->addTag($tagName);
                    $connection->assignTagToVideo($videoId, $newTagId);
                }
            }


            header('Location: /');
        }
    }
}