<?php
namespace engine\controllers;
use config\Config;

/**
 * Created by PhpStorm.
 * UserData: smile
 * Date: 20.06.17
 * Time: 16:22
 *
 * Хендлер отображения страниц сайта.
 * Метод handle() принимает в себя параметр page из $_GET['page'].
 * Проверяет наличие роутинга к классу этой страницы и подгружает ее, выполняя абстрактный метод handle из класса AbstractPage.
 * В случае отсутствия страницы, показывает 404.
 */
class ActionHandler {
    public function execute($action) {
        if(array_key_exists($action, Config::$actions)){
            $actionClass = Config::$actions[$action]; // берем указанный PHP по названию раздела для выполнения
            /** @var AbstractController $controller */
            $controller = new $actionClass(); //создаем новый объект
            $controller->execute(); //вызываем абстрактный метод handle()
        }
    }
}