<?php
namespace engine\handlers;

use config\Config;
use engine\handlers\pages\NotFoundPage;

/**
 * Created by PhpStorm.
 * UserData: smile
 * Date: 20.06.17
 * Time: 16:22
 *
 * Хендлер отображения страниц сайта.
 * Метод handle() принимает в себя параметр page из $_GET['page'].
 * Проверяет наличие роутинга к классу этой страницы и подгружает ее, выполняя абстрактный метод handle из класса AbstractPage.
 * В случае отсутствия страницы, показывает 404.
 */
class PageHandler {
    public function handle($page) {
        if(array_key_exists($page, Config::$siteRoutes)){
            $pageClass = Config::$siteRoutes[$page]; // берем указанный PHP по названию раздела для выполнения
            $page = new $pageClass(); //создаем новый объект
            return $page->handle(); //вызываем абстрактный метод handle()
        } else {
            $notFound = new NotFoundPage();
            return $notFound->handle();
        }
    }
}