<?php
namespace engine\handlers\pages;

use db\Connection;

/**
 * Created by PhpStorm.
 * UserData: smile
 * Date: 20.06.17
 * Time: 16:54
 */
class AddPage extends AbstractPage {

    private $html = "";

    private $form_html = "template/login/form.html";

    private $dash_html = "template/login/dash.html";

    private $add_form_html = "template/admin/form/add.html";

    private $tags_form = "template/admin/elements/tag_form/tag_form.html";

    private $tag_item = "template/admin/elements/tag_form/tag_item.html";

    public function handle() {
    
        return $this->replaceVars();
    }



    private function replaceVars(){
        $result = $this->getIndexPage();

        if(isset($_SESSION['loggedIn']))
        {
            $result = str_replace('{form}', file_get_contents($this->dash_html), $result);
        } else {
            $result = str_replace('{form}', file_get_contents($this->form_html), $result);
        }


        $result =  str_replace("{content}", file_get_contents($this->add_form_html), $result);

        return $this->replaceAdminVar($result);
    }
}

?>