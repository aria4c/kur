<?php
namespace engine\handlers\pages;

use db\Connection;

/**
 * Created by PhpStorm.
 * UserData: smile
 * Date: 20.06.17
 * Time: 16:54
 */
class EditPage extends AbstractPage {

    private $html = "";

    private $form_html = "template/login/form.html";

    private $dash_html = "template/login/dash.html";

    private $edit_form_html = "template/admin/form/edit.html";

    private $tags_input = "template/admin/elements/tags_input.html";

    /** @var  Connection */
    protected $connection;

    public function handle() {
        $this->connection = new Connection();
        $this->generateEditForm();
        return $this->replaceVars();
    }

    public function getTagsHtml(){
        $tags = '';
        $tagsList = $this->connection->getTagByVideo($_GET['id']);
        if($tagsList)
        {
            foreach ($tagsList as $item)
            {
                $tags.=$item['name'].",";
            }
        }

        return str_replace('{tags}', rtrim($tags, ','), file_get_contents($this->tags_input));
    }

    public function generateEditForm()
    {

        $this->html=file_get_contents($this->edit_form_html);
        $this->html = $this->replaceAdminVar($this->html, $_GET['id']);
        $video = $this->connection->getVideoById($_GET['id']);
        $this->html = str_replace('{name}', $video['name'], $this->html);
        $this->html = str_replace('{link}', $video['link'], $this->html);
        $this->html = str_replace('{content}', $video['content'], $this->html);
        $this->html = str_replace('{tags}', $this->getTagsHtml(), $this->html);
    }


    private function replaceVars(){
        $result = $this->getIndexPage();

        if(isset($_SESSION['loggedIn']))
        {
            $result = str_replace('{form}', file_get_contents($this->dash_html), $result);
        } else {
            $result = str_replace('{form}', file_get_contents($this->form_html), $result);
        }


        $result =  str_replace("{content}", $this->html, $result);

        return $this->replaceAdminVar($result);
    }
}

?>