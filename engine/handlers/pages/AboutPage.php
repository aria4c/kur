<?php
namespace engine\handlers\pages;

/**
 * Created by PhpStorm.
 * UserData: smile
 * Date: 20.06.17
 * Time: 16:54
 */
class AboutPage extends AbstractPage {

    private $html = "template/about.html";

    public function handle() {
        return $this->replaceVars();
    }

    private function replaceVars(){
        return str_replace("{content}", file_get_contents($this->html), $this->getIndexPage());
    }
}