<?php
namespace engine\handlers\pages;

/**
 * Created by PhpStorm.
 * UserData: smile
 * Date: 20.06.17
 * Time: 17:29
 */
class ContactPage extends AbstractPage {

    private $html = "template/contact.html";

    public function handle() {
        return $this->replaceVars();
    }

    private function replaceVars(){
        return str_replace("{content}", file_get_contents($this->html), $this->getIndexPage());
    }
}