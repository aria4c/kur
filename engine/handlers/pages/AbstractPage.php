<?php
namespace engine\handlers\pages;

use config\Config;
/**
 * Created by PhpStorm.
 * UserData: smile
 * Date: 20.06.17
 * Time: 16:24
 *
 *  Абстрактный класс любой страницы. Подгружает стили, навбар, тело и футер, имеет в себе асбтрактный метод handle(),
 *  который служит реалзиацией отображения раздела сайта, а также заменой переменных на актуальные.
 */
abstract class AbstractPage {

    private $indexPage; //главная страница

    function __construct() {
        $this->indexPage = file_get_contents("template/index.html");
    }

    public abstract function handle();

    public function getIndexPage() {
        return $this->indexPage;
    }

    public function replaceAdminVar($html, $id=null) {
        $username = '';
        foreach (Config::$adminVars as $var => $template)
        {
            $replace = '';
            if(isset($_SESSION['loggedIn']))
            {
                $replace=file_get_contents($template);
            }

            $html = str_replace($var, $replace, $html);
        }
        if(isset($_SESSION['loggedIn']))
        {
            $username = $_SESSION['username'];
            if($id)
            {
                $html = str_replace('{{id}}', $id, $html);
            }
        }

        return str_replace('{%%username%%}', $username, $html);
    }

    public function escapeAdminVar($replacement, $fileIn, $variable){
        return str_replace($replacement, $fileIn, $variable);
    }

    public function replaceVar($replacement, $fileIn, $variable) {
        return str_replace($replacement, $fileIn, $variable);
    }
}