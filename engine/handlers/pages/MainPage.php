<?php

namespace engine\handlers\pages;

use db\Connection;

/**
 * Created by PhpStorm.
 * UserData: smile
 * Date: 20.06.17
 * Time: 16:54
 */
class MainPage extends AbstractPage
{

    private $html = "";

    private $video_block_html = "template/video/item.html";

    private $video_list = "";

    private $video_list_html = "template/video/list.html";

    private $form_html = "template/login/form.html";

    private $dash_html = "template/login/dash.html";

    public function handle()
    {
        $this->generateBlocks();
        return $this->replaceVars();
    }

    public function generateBlocks()
    {
        $connection = new Connection();
        $videosCollection = $connection->getVideos();
        foreach ($videosCollection as $video) {
            $videoBlock = file_get_contents($this->video_block_html);
            $tagsCollection = $connection->getTagByVideo((int)$video['id']);
            if ($tagsCollection) {
                $tagsList = '';
                while ($tag = mysqli_fetch_assoc($tagsCollection)) {
                    $tagsList .= $this->generateTagHtml($tag['tag_id'], $tag['name']);
                }
                $videoBlock = str_replace('{tag}', $tagsList, $videoBlock);
            } else {
                $videoBlock = str_replace('{tag}', '', $videoBlock);
            }

            $videoBlock = str_replace("{title}", $video['name'], $videoBlock);
            $videoBlock = str_replace("{link}", $video['link'], $videoBlock);
            $videoBlock = str_replace("{content}", $video['content'], $videoBlock);
            $videoBlock = $this->replaceAdminVar($videoBlock, $video['id']);
            $this->video_list .= $videoBlock;
        }

        $this->html = str_replace("{content}", $this->video_list, file_get_contents($this->video_list_html));
    }

    public function generateTagHtml($id, $name)
    {
        return "<a href='/index.php?page=tag_search&tag_id=$id'> $name </a>"; // как две ссылки сделать?
    }

    private function replaceVars()
    {
        $result = $this->getIndexPage();

        if (isset($_SESSION['loggedIn'])) {
            $result = str_replace('{form}', file_get_contents($this->dash_html), $result);
        } else {
            $result = str_replace('{form}', file_get_contents($this->form_html), $result);
        }
        $result = str_replace("{content}", $this->html, $result);

        return $this->replaceAdminVar($result);
    }
}

